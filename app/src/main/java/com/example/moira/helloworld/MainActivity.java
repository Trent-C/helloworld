package com.example.moira.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.hardware.camera2.CameraAccessException;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
//import org.opencv.imgproc.*;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
//#include opencv2/imgproc/imgproc.hpp;
//#include opencv2/highgui/highgui.hpp;

//import android.widget.TextView;
//import android.os.Handler;
//import java.util.Calendar;

public class MainActivity extends AppCompatActivity
        implements CvCameraViewListener2
{
    private static final String TAG = "OVCSample::Activity";
    private CameraBridgeViewBase mOpenCvCameraView;
    //private Mat mRgba;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status)
        {
            switch (status)
            {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    //Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Log that the oncreate function has be called
        Log.i(TAG, "called onCreate");

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Link app with the activity_main layout xml so it knows how the screen should look
        setContentView(R.layout.activity_main);

        // Link the open CV camera view object to the camera hardware as defined in the layout file.
        // This uses the findViewById function which will scan the xml file for tutorial1_activity_java_surface_view
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.tutorial1_activity_java_surface_view);

        // Now that camera object has been linked to theis variable we set its visibility to true
        // so camera can be seen on screen.
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

        // Now we must tell the camera that every time a frame is recorded it should call the onframe function
        // defined later. Since the onframe function is part of the class we set  the listner to 'true'
        mOpenCvCameraView.setCvCameraViewListener(this);

        //TextView text_view_ = (TextView) findViewById(R.id.textView);
        //TextView text_view_2_ = (TextView) findViewById(R.id.textView2);
        //text_view_.setText("hello2");
        //text_view_2_.setText("world2");

        //mHandler.postDelayed(mUpdateUITimerTask, 1000);
    }

    @Override
    public void onPause() {
        super.onPause();
        // When the app is paused and you are currently accessing the camera, disable the camera.
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        // If the OpenCVLoader is baseloader is not connected make a connection to the baseloader callback
        // in asyncronous mode.
        if (!OpenCVLoader.initDebug())
        {
            Log.d(TAG, "Internal OpenCV library not found. Using openCV manager");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        }
        else
        {
            Log.d(TAG, "OpenCV library found inside package . Using It !");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy()
    {
        super.onDestroy();
        // When the app is destroyed and you are currently accessing the camera, disable the camera.
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height)
    {
        // This can be left blank, just here to compile properly
    }

    public void onCameraViewStopped()
    {
        // This can be left blank, just here to compile properly
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame)
    {
        // --- FLIP THE SCREEN ---
        Mat mRgba;
        // RED GREEN BLUE ARRAY
        mRgba = inputFrame.rgba();
        // Transpose .t();
        // Mat mRgbaT = mRgba.t();
        // flip(Mat src, Mat dst, int flipCode);
        // Core.flip(mRgba.t(), mRgbaT, 1);
        // Core.flip(mRgba.t(), mRgba, 1);
        // resize(Mat src, Mat dst, Size dsize);
        // Size dsize = mRgba.size();       //640,480
        // Size dsize;
        // dsize = Size(double 640,double 480);
        // Imgproc.resize(mRgbaT, mRgbaT, mRgba.size());
        // return inputFrame.rgba();

        // --- CONVERT TO HSV COLOUR SPACE ---
        Mat hsv = convert2HSV(mRgba);

        // --- THRESHOLDS ---
        Mat othreshold = ballThresholding(hsv);
        Mat kthreshold = obsThresholding(hsv);

        // --- Remove noise (Erode and Dilate)---
        Mat oblobs = removeNoise(othreshold);
        Mat kblobs = removeNoise(kthreshold);

        // --- Fill blobs (Dilate and Erode) ---
        oblobs = fillBlobs(oblobs);
        kblobs = fillBlobs(kblobs);

        // --- BALL output ---
        List<MatOfPoint> ocontours = getContours(oblobs);
        if (ocontours.size()>=1) {
            Rect orect = blobs2rect(ocontours);
            Imgproc.rectangle(mRgba, new Point(orect.x, orect.y), new Point(orect.x + orect.width, orect.y + orect.height), new Scalar(255, 0, 0, 255), 3);
            double[] ballLocation = getballDistanceAngle(orect);
            String string1 = Arrays.toString(ballLocation);
            Imgproc.putText(mRgba, string1, new Point(orect.x, orect.y), Core.FONT_HERSHEY_PLAIN, 2.0, new Scalar(180, 0, 0));
        }

        // --- OBSTACLES output ---
        List<MatOfPoint> kcontours = getContours(kblobs);
        //For each contour found
        if (kcontours.size()>=1) {
            for (int i = 0; i < kcontours.size(); i++) {
                Rect krect = blobs2rect(kcontours);
                Imgproc.rectangle(mRgba, new Point(krect.x, krect.y), new Point(krect.x + krect.width, krect.y + krect.height), new Scalar(0, 255, 255, 255), 3);
                double[] obsLocation = getObsDistanceAngle(krect);
                String string1 = Arrays.toString(obsLocation);
                Imgproc.putText(mRgba, string1, new Point(krect.x, krect.y), Core.FONT_HERSHEY_PLAIN, 2.0, new Scalar(180, 0, 0));
            }
        }
        return mRgba;
    }

    public Mat convert2HSV(Mat src){
        Mat dst = new Mat();
        Imgproc.cvtColor(src, dst, Imgproc.COLOR_RGB2HSV); //SHOULD THIS BE BGR2HSV??
        return dst;
    }

    public Mat ballThresholding(Mat src){
        Mat dst = new Mat();
        Mat lower_othreshold = new Mat();
        Mat upper_othreshold = new Mat();
        Core.inRange(src, new Scalar(0,130,100), new Scalar(10,255,255), lower_othreshold);
        Core.inRange(src, new Scalar(160,130,100), new Scalar(179,255,255), upper_othreshold);
        Core.addWeighted(lower_othreshold,1.0,upper_othreshold,1.0,0.0,dst);
        Imgproc.GaussianBlur(dst,dst,new Size(9,9),2,2);
        return dst;
    }

    public Mat obsThresholding(Mat src){
        Mat dst = new Mat();     //hsv;
        Core.inRange(src, new Scalar(0,0,0), new Scalar(255,255,60), dst);
        return dst;
    }

    public Mat removeNoise(Mat src){
        Mat dst = new Mat();
        int elementSize = 5;
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*elementSize + 1, 2*elementSize+1));
        Imgproc.erode(src, dst, element);
        Imgproc.dilate(dst, dst, element);
        return dst;
    }

    public Mat fillBlobs(Mat src){
        Mat dst = new Mat();
        int elementSize = 5;
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*elementSize + 1, 2*elementSize+1));
        Imgproc.dilate(src, dst, element);
        Imgproc.erode(dst, dst, element);
        return dst;
    }

    public List<MatOfPoint> getContours(Mat blobs){
        Mat contoursFrame = blobs;
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(contoursFrame, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        return contours;
    }

    public Rect blobs2rect(List<MatOfPoint> ocontours){
        double olargestArea = 0;
        //double onxtLargestArea = 0;
        int olargestContourIndex = 0;
        //int onxtLargestContourIndex = 0;
        MatOfPoint2f oapproxCurve = new MatOfPoint2f();

        for (int i = 0; i < ocontours.size(); i++) {
            double a = Imgproc.contourArea(ocontours.get(i), false);  //Find the largest area of contour
            if (a > olargestArea) {
                //onxtLargestArea = olargestArea;
                //onxtLargestContourIndex = olargestContourIndex;
                olargestArea = a;
                olargestContourIndex = i;
            }
        }
        //Convert contours(i) from MatOfPoint to MatOfPoint2f
        MatOfPoint2f ocontour2f = new MatOfPoint2f(ocontours.get(olargestContourIndex).toArray());
        //Processing on mMOP2f1 which is in type MatOfPoint2f
        double oapproxDistance = Imgproc.arcLength(ocontour2f, true) * 0.02;
        Imgproc.approxPolyDP(ocontour2f, oapproxCurve, oapproxDistance, true);
        //Convert back to MatOfPoint
        MatOfPoint opoints = new MatOfPoint(oapproxCurve.toArray());
        // Get bounding rect of contour
        Rect orect = Imgproc.boundingRect(opoints);
        return orect;

    }

    public double[] getballDistanceAngle(Rect orect){
        double focalLength = 3.7;
        double sensorHeight = 4.54;
        //double sensorWidth = 3.42;
        double diagonalFOV = 1.31;
        double diagonalPixels = 800;
        double cameraCentrePixel = 480 / 2;
        // --- Resolution ---
        //double resolutionWidth = 480;
        double resolutionHeight = 640;
        // --- Object Dimensions ---
        //double ballWidth = 47;
        double ballHeight = 47;
        double odist = ((focalLength * ballHeight * resolutionHeight) / (orect.height * sensorHeight));
        odist = roundTwoDecimals(odist);
        double ballCentrePixel = orect.y + ((orect.height) / 2);
        double oAngle = ((cameraCentrePixel - ballCentrePixel) * (diagonalFOV)) / (diagonalPixels);
        oAngle = roundTwoDecimals(oAngle);
        double[] ballDistAng = {odist,oAngle};
        return ballDistAng;
    }

    public double[] getObsDistanceAngle(Rect krect){
        double focalLength = 3.7;
        double sensorHeight = 4.54;
        //double sensorWidth = 3.42;
        double diagonalFOV = 1.31;
        double diagonalPixels = 800;
        double cameraCentrePixel = 480 / 2;
        // --- Resolution ---
        //double resolutionWidth = 480;
        double resolutionHeight = 640;
        // --- Object Dimensions ---
        double obsWidth = 220;
        double obsHeight = 150;
        // Dist = (FocalLength * ObjectWidth * PixelsWide) / (ObjectHeight * SensorHeight)
        double obsDist = (focalLength * obsHeight * resolutionHeight) / (krect.height * sensorHeight);
        obsDist = roundTwoDecimals(obsDist);
        double obsCentrePixel = krect.y + ((krect.height) / 2);
        double obsAngle = ((cameraCentrePixel - obsCentrePixel) * (diagonalFOV)) / (diagonalPixels);
        obsAngle = roundTwoDecimals(obsAngle);
        double[] obsDistAng = {obsDist,obsAngle};
        return obsDistAng;
    }

    public double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
    }
}

